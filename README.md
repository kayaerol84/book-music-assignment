# Books and Albums 

## How to run the application

### API 

First 

* mvn clean install

Then  

* mvn spring-boot:run

or 

* Right click on BookMusicAssignmentApplication.java and Run

## Health of API 

* You can check the various available endpoints via this link http://localhost:8080/actuator
* Health check http://localhost:8080/actuator/health
* Metrics http://localhost:8080/actuator/metrics 
 
## API Documentation

* mvn clean verify 
* Go to /target/generated-docs directory
* Open up `api-guide.html`
http://localhost:63342/book-music-assignment/target/generated-docs/api-guide.html
* Please refer to this link above for how to get books and music albums 
