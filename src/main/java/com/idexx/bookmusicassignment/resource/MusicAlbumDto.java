package com.idexx.bookmusicassignment.resource;

import com.idexx.bookmusicassignment.domain.MusicAlbum;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MusicAlbumDto {
    private String title;
    private String artist;
    public static MusicAlbumDto map(MusicAlbum musicAlbum) {
        return MusicAlbumDto.builder()
                .title(musicAlbum.getTitle())
                .artist(musicAlbum.getArtist())
                .build();
    }
}
