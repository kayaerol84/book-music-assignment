package com.idexx.bookmusicassignment.resource;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class ResultDto {

    List<BookDto> books;
    List<MusicAlbumDto> albums;
}
