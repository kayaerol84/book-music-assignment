package com.idexx.bookmusicassignment.resource;

import com.idexx.bookmusicassignment.domain.Book;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class BookDto {
    private String title;
    private List<String> authors;
    public static BookDto map(Book b) {
        return BookDto.builder()
                .title(b.getTitle())
                .authors(b.getAuthors())
                .build();
    }
}
