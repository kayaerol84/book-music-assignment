package com.idexx.bookmusicassignment.resource;

import com.idexx.bookmusicassignment.domain.Book;
import com.idexx.bookmusicassignment.domain.MusicAlbum;
import com.idexx.bookmusicassignment.service.BookService;
import com.idexx.bookmusicassignment.service.MusicAlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/store")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StoreResource {

    private final MusicAlbumService musicAlbumService;
    private final BookService bookService;

    @GetMapping
    @ResponseBody
    public ResultDto getStoreResults(@RequestParam final String input) throws IOException {

        final List<Book> books = bookService.getBooks(input);
        final List<MusicAlbum> albums = musicAlbumService.getMusicAlbums(input);

        return ResultDto.builder()
                .books(books.stream().map(BookDto::map).collect(Collectors.toList()) )
                .albums(albums.stream().map(MusicAlbumDto::map).collect(Collectors.toList()))
                .build();
    }
}
