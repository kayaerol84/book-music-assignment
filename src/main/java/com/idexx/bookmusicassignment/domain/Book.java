package com.idexx.bookmusicassignment.domain;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class Book {
    private final String title;
    private final List<String> authors;
}
