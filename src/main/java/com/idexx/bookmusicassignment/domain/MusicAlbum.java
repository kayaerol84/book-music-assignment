package com.idexx.bookmusicassignment.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MusicAlbum {
    private final String title;
    private final String artist;
}
