package com.idexx.bookmusicassignment.service;

import com.idexx.bookmusicassignment.api.AlbumsResult;
import com.idexx.bookmusicassignment.api.ApiClient;
import com.idexx.bookmusicassignment.api.ITunesAlbum;
import com.idexx.bookmusicassignment.domain.MusicAlbum;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MusicAlbumService {

    @Value("${itunes.search.entity}")
    private String searchEntity;
    private final ApiClient apiClient;

    public List<MusicAlbum> getMusicAlbums(final String input) throws IOException {
        final AlbumsResult albums = apiClient.callITunesApi(input, searchEntity);
        return albums.getMusicAlbums().stream()
                .map(ITunesAlbum::map)
                .collect(Collectors.toList());
    }
}
