package com.idexx.bookmusicassignment.service;

import com.idexx.bookmusicassignment.api.ApiClient;
import com.idexx.bookmusicassignment.api.BooksResult;
import com.idexx.bookmusicassignment.api.GoogleBook;
import com.idexx.bookmusicassignment.domain.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BookService {
    private final ApiClient apiClient;

    public List<Book> getBooks(final String input) throws IOException {
        final BooksResult books = apiClient.callGoogleBooksApi(input);

        final List<GoogleBook> googleBooks = books.getBooks();
        return googleBooks.stream()
                .map(GoogleBook::map)
                .collect(Collectors.toList());
    }
}
