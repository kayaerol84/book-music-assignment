package com.idexx.bookmusicassignment.api;

import com.idexx.bookmusicassignment.domain.Book;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class GoogleBook {
    private final String title;
    private final List<String> authors;

    public static Book map(GoogleBook googleBook) {
        return Book.builder()
                .title(googleBook.getTitle())
                .authors(googleBook.getAuthors()).build();
    }
}
