package com.idexx.bookmusicassignment.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApiClient {

    private final RestTemplate restTemplate;
    @Value("${google.books.url}")
    private String googleBooksUrl;
    @Value("${itunes.url}")
    private String iTunesUrl;
    @Value("${itunes.return.limit}")
    private int iTunesLimit;
    @Value("${google.return.limit}")
    private int googleBooksLimit;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @HystrixCommand(fallbackMethod = "reliableGoogleBooks")
    public BooksResult callGoogleBooksApi(final String input) throws IOException {
        final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(googleBooksUrl)
                .queryParam("q", input)
                .queryParam("maxResults", googleBooksLimit);

        final String responseJson = restTemplate.getForObject(uriBuilder.toUriString(), String.class);

        final List<GoogleBook> googleBooks = new ArrayList<>();
        final JsonNode rootJson = objectMapper.readTree(responseJson);
        if (!isGoogleBooksApiResponseEmpty(rootJson)) {
            final ArrayNode items = (ArrayNode) rootJson.path("items");
            getStreamFor(items).forEach(json -> googleBooks.add(getGoogleBook(json)));
        }

        return BooksResult.builder().books(googleBooks).build();
    }

    @HystrixCommand(fallbackMethod = "reliableITunes")
    public AlbumsResult callITunesApi( final String input, final String... entities) throws IOException {
        final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(iTunesUrl)
                .queryParam("term", input)
                .queryParam("entity", entities)
                .queryParam("limit", iTunesLimit);

        final String responseJson = restTemplate.getForObject(uriBuilder.toUriString(), String.class);

        final List<ITunesAlbum> iTunesAlbums = new ArrayList<>();
        final JsonNode rootJson = objectMapper.readTree(responseJson);
        if (!isITunesApiResponseEmpty(rootJson)) {
            final ArrayNode items = (ArrayNode) rootJson.path("results");
            getStreamFor(items).forEach(json -> iTunesAlbums.add(getItunesAlbum(json)));
        }
        return AlbumsResult.builder().musicAlbums(iTunesAlbums).build();
    }

    private static boolean isITunesApiResponseEmpty(final JsonNode rootJson) {
        return rootJson.get("resultCount").asInt() == 0;
    }
    private static boolean isGoogleBooksApiResponseEmpty(final JsonNode rootJson) {
        return rootJson.get("totalItems").asInt() == 0;
    }

    private static Stream<JsonNode> getStreamFor(final ArrayNode items) {
        return StreamSupport.stream(items.spliterator(), false);
    }

    private static GoogleBook getGoogleBook(final JsonNode json) {
        final JsonNode volumeInfo = json.findValue("volumeInfo");
        final String title = volumeInfo.findValue("title").asText();
        final ArrayNode authors = (ArrayNode) volumeInfo.findValue("authors");
        final List<String> authorList = getStreamFor(authors).map(JsonNode::asText).collect(Collectors.toList());
        return GoogleBook.builder().authors(authorList).title(title).build();
    }

    private static ITunesAlbum getItunesAlbum(final JsonNode json) {
        final String title = json.findValue("collectionName").asText();
        final String artistName = json.findValue("artistName").asText();
        return ITunesAlbum.builder().artist(artistName).title(title).build();
    }

    public String reliableGoogleBooks(){
        return "Google Books API not available";
    }

    public String reliableITunes(){
        return "iTunes API not available";
    }
}
