package com.idexx.bookmusicassignment.api;

import com.idexx.bookmusicassignment.domain.MusicAlbum;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ITunesAlbum {

    private final String title;
    private final String artist;

    public static MusicAlbum map(ITunesAlbum iTunesAlbum) {
        return MusicAlbum.builder()
                .artist(iTunesAlbum.getArtist())
                .title(iTunesAlbum.getTitle())
                .build();
    }
}
