package com.idexx.bookmusicassignment.api;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class AlbumsResult {
    List<ITunesAlbum> musicAlbums;
}
