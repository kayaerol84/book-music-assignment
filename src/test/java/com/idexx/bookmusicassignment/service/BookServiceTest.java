package com.idexx.bookmusicassignment.service;

import com.idexx.bookmusicassignment.api.ApiClient;
import com.idexx.bookmusicassignment.api.BooksResult;
import com.idexx.bookmusicassignment.api.GoogleBook;
import com.idexx.bookmusicassignment.domain.Book;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    @Mock
    private ApiClient apiClient;

    @InjectMocks
    private BookService bookService;
    private BooksResult booksResult;
    @Before
    public void setUp() {
         booksResult = BooksResult.builder()
                 .books(Arrays.asList(
                         GoogleBook.builder().title("George Orwell: In front of your nose, 1946-1950").authors(Arrays.asList("George Orwell", "Sonia Orwell", "Ian Angus")).build(),
                         GoogleBook.builder().title("Animal Farm").authors(Arrays.asList("George Orwell")).build(),
                         GoogleBook.builder().title("1984").authors(Arrays.asList("George Orwell")).build()))
                 .build();
    }

    @Test
    public void getBooks() throws IOException {
        String input = "orwell";
        when(apiClient.callGoogleBooksApi(input)).thenReturn(booksResult);

        List<Book> books = bookService.getBooks(input);

        assertEquals(booksResult.getBooks().size(),  books.size());
        assertEquals(booksResult.getBooks().get(0).getTitle(),  books.get(0).getTitle());
        assertEquals(booksResult.getBooks().get(0).getAuthors(),  books.get(0).getAuthors());

        assertEquals("George Orwell: In front of your nose, 1946-1950", books.get(0).getTitle());
        assertEquals(3, books.get(0).getAuthors().size());
    }
}