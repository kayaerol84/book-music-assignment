package com.idexx.bookmusicassignment.service;

import com.idexx.bookmusicassignment.api.AlbumsResult;
import com.idexx.bookmusicassignment.api.ApiClient;
import com.idexx.bookmusicassignment.api.ITunesAlbum;
import com.idexx.bookmusicassignment.domain.MusicAlbum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MusicAlbumServiceTest {

    @Mock
    private ApiClient apiClient;
    @InjectMocks
    private MusicAlbumService musicAlbumService;
    private AlbumsResult albumsResult;
    private final String searchEntity = "album";
    @Before
    public void setUp() {

        ReflectionTestUtils.setField(musicAlbumService, "searchEntity", searchEntity);

        albumsResult = AlbumsResult.builder()
                .musicAlbums(Arrays.asList(
                        ITunesAlbum.builder().title("The Platinum Collection (Greatest Hits I, II & III)").artist("Queen").build(),
                        ITunesAlbum.builder().title("Bohemian Rhapsody (The Original Soundtrack)").artist("Queen").build(),
                        ITunesAlbum.builder().title("Greatest Hits").artist("Queen").build()))
                .build();
    }

    @Test
    public void getMusicAlbums() throws IOException {
        when(apiClient.callITunesApi("Queen", searchEntity)).thenReturn(albumsResult);
        List<MusicAlbum> albums = musicAlbumService.getMusicAlbums("Queen");

        assertEquals(albumsResult.getMusicAlbums().size(), albums.size());
        assertEquals(albumsResult.getMusicAlbums().get(0).getArtist(), albums.get(0).getArtist());
        assertEquals(albumsResult.getMusicAlbums().get(0).getTitle(), albums.get(0).getTitle());
    }
}
