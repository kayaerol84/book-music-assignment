package com.idexx.bookmusicassignment.api;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApiClientTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private ApiClient apiClient;
    private String googleUrl = "https://www.googleapis.com/books/v1/volumes?q=George%20Orwell&maxResults=5";
    private String iTunesUrl = "https://itunes.apple.com/search?term=Queen&entity=album&limit=5";

    @Before
    public void setup (){
        ReflectionTestUtils.setField(apiClient, "googleBooksUrl", "https://www.googleapis.com/books/v1/volumes");
        ReflectionTestUtils.setField(apiClient, "googleBooksLimit", 5);

        ReflectionTestUtils.setField(apiClient, "iTunesUrl", "https://itunes.apple.com/search");
        ReflectionTestUtils.setField(apiClient, "iTunesLimit", 5);
    }
    @Test
    public void callGoogleBooksApi() throws IOException {
        when(restTemplate.getForObject(googleUrl, String.class)).thenReturn(MockJsonResponse.getGoogleBooksResponse());
        BooksResult books = apiClient.callGoogleBooksApi("George Orwell");

        assertEquals(5, books.getBooks().size());
        assertEquals("Animal farm", books.getBooks().get(0).getTitle());
    }

    @Test
    public void callITunesApi() throws IOException {
        when(restTemplate.getForObject(iTunesUrl, String.class)).thenReturn(MockJsonResponse.getITunesResponse());
        AlbumsResult albums = apiClient.callITunesApi("Queen", "album");

        assertEquals(5, albums.getMusicAlbums().size());
        assertEquals("The Platinum Collection (Greatest Hits I, II & III)", albums.getMusicAlbums().get(0).getTitle());
    }

    @Test
    public void callITunesApi_shouldReturnEmptyList_whenNothingReturn() throws IOException {
        when(restTemplate.getForObject(iTunesUrl, String.class)).thenReturn(MockJsonResponse.getEmptyITunesResponse());
        AlbumsResult albums = apiClient.callITunesApi("Queen", "album");

        assertEquals(0, albums.getMusicAlbums().size());
    }
    @Test
    public void callGoogleBooksApi_shouldReturnEmptyList_whenNothingReturn() throws IOException {
        when(restTemplate.getForObject(googleUrl, String.class)).thenReturn(MockJsonResponse.getEmptyGoogleResponse());
        BooksResult books = apiClient.callGoogleBooksApi("George Orwell");

        assertEquals(0, books.getBooks().size());
    }

    @Test
    public void reliableGoogleBooks() {
    }

    @Test
    public void reliableITunes() {
    }
}
