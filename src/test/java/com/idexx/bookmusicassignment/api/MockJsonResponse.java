package com.idexx.bookmusicassignment.api;

public class MockJsonResponse {
    public static String getITunesResponse() {
        return "\n" +
                "\n" +
                "\n" +
                "{\n" +
                " \"resultCount\":5,\n" +
                " \"results\": [\n" +
                "{\"wrapperType\":\"collection\", \"collectionType\":\"Album\", \"artistId\":3296287, \"collectionId\":1440650428, \"amgArtistId\":5205, \"artistName\":\"Queen\", \"collectionName\":\"The Platinum Collection (Greatest Hits I, II & III)\", \"collectionCensoredName\":\"The Platinum Collection (Greatest Hits I, II & III)\", \"artistViewUrl\":\"https://music.apple.com/us/artist/queen/3296287?uo=4\", \"collectionViewUrl\":\"https://music.apple.com/us/album/the-platinum-collection-greatest-hits-i-ii-iii/1440650428?uo=4\", \"artworkUrl60\":\"https://is1-ssl.mzstatic.com/image/thumb/Music128/v4/9e/58/3b/9e583b8c-785e-64ee-ce3f-dc365f263861/source/60x60bb.jpg\", \"artworkUrl100\":\"https://is1-ssl.mzstatic.com/image/thumb/Music128/v4/9e/58/3b/9e583b8c-785e-64ee-ce3f-dc365f263861/source/100x100bb.jpg\", \"collectionPrice\":24.99, \"collectionExplicitness\":\"notExplicit\", \"trackCount\":51, \"copyright\":\"℗ 2014 Hollywood Records, Inc.\", \"country\":\"USA\", \"currency\":\"USD\", \"releaseDate\":\"2000-11-13T08:00:00Z\", \"primaryGenreName\":\"Rock\"}, \n" +
                "{\"wrapperType\":\"collection\", \"collectionType\":\"Album\", \"artistId\":3296287, \"collectionId\":1434899831, \"amgArtistId\":5205, \"artistName\":\"Queen\", \"collectionName\":\"Bohemian Rhapsody (The Original Soundtrack)\", \"collectionCensoredName\":\"Bohemian Rhapsody (The Original Soundtrack)\", \"artistViewUrl\":\"https://music.apple.com/us/artist/queen/3296287?uo=4\", \"collectionViewUrl\":\"https://music.apple.com/us/album/bohemian-rhapsody-the-original-soundtrack/1434899831?uo=4\", \"artworkUrl60\":\"https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/f0/f9/9f/f0f99fa4-3685-cf99-9676-3b303e22fbf7/source/60x60bb.jpg\", \"artworkUrl100\":\"https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/f0/f9/9f/f0f99fa4-3685-cf99-9676-3b303e22fbf7/source/100x100bb.jpg\", \"collectionPrice\":11.99, \"collectionExplicitness\":\"notExplicit\", \"trackCount\":22, \"copyright\":\"℗ 2018 Hollywood Records, Inc.\", \"country\":\"USA\", \"currency\":\"USD\", \"releaseDate\":\"2018-10-19T07:00:00Z\", \"primaryGenreName\":\"Soundtrack\"}, \n" +
                "{\"wrapperType\":\"collection\", \"collectionType\":\"Album\", \"artistId\":3296287, \"collectionId\":1422650667, \"amgArtistId\":5205, \"artistName\":\"Queen\", \"collectionName\":\"Greatest Hits\", \"collectionCensoredName\":\"Greatest Hits\", \"artistViewUrl\":\"https://music.apple.com/us/artist/queen/3296287?uo=4\", \"collectionViewUrl\":\"https://music.apple.com/us/album/greatest-hits/1422650667?uo=4\", \"artworkUrl60\":\"https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/30/31/ec/3031ec3e-5e80-3562-7c16-d77632aa27ba/source/60x60bb.jpg\", \"artworkUrl100\":\"https://is3-ssl.mzstatic.com/image/thumb/Music118/v4/30/31/ec/3031ec3e-5e80-3562-7c16-d77632aa27ba/source/100x100bb.jpg\", \"collectionPrice\":10.99, \"collectionExplicitness\":\"notExplicit\", \"trackCount\":18, \"copyright\":\"This Compilation ℗ 2014 Hollywood Records, Inc.\", \"country\":\"USA\", \"currency\":\"USD\", \"releaseDate\":\"1981-10-26T08:00:00Z\", \"primaryGenreName\":\"Rock\"}, \n" +
                "{\"wrapperType\":\"collection\", \"collectionType\":\"Album\", \"artistId\":3296287, \"collectionId\":1440650816, \"amgArtistId\":5205, \"artistName\":\"Queen\", \"collectionName\":\"A Night at the Opera (Deluxe Edition)\", \"collectionCensoredName\":\"A Night at the Opera (Deluxe Edition)\", \"artistViewUrl\":\"https://music.apple.com/us/artist/queen/3296287?uo=4\", \"collectionViewUrl\":\"https://music.apple.com/us/album/a-night-at-the-opera-deluxe-edition/1440650816?uo=4\", \"artworkUrl60\":\"https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/49/34/3a/49343ad7-1872-16b4-e964-13bb2be937d5/source/60x60bb.jpg\", \"artworkUrl100\":\"https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/49/34/3a/49343ad7-1872-16b4-e964-13bb2be937d5/source/100x100bb.jpg\", \"collectionPrice\":11.99, \"collectionExplicitness\":\"notExplicit\", \"trackCount\":18, \"copyright\":\"℗ 2014 Hollywood Records, Inc.\", \"country\":\"USA\", \"currency\":\"USD\", \"releaseDate\":\"1975-11-21T08:00:00Z\", \"primaryGenreName\":\"Rock\"}, \n" +
                "{\"wrapperType\":\"collection\", \"collectionType\":\"Album\", \"artistId\":1419227, \"collectionId\":780330041, \"artistName\":\"Beyoncé\", \"collectionName\":\"BEYONCÉ\", \"collectionCensoredName\":\"BEYONCÉ\", \"artistViewUrl\":\"https://music.apple.com/us/artist/beyonc%C3%A9/1419227?uo=4\", \"collectionViewUrl\":\"https://music.apple.com/us/album/beyonc%C3%A9/780330041?uo=4\", \"artworkUrl60\":\"https://is2-ssl.mzstatic.com/image/thumb/Music6/v4/17/84/3a/17843a6d-1f2b-7e1e-a39f-3ff865110993/source/60x60bb.jpg\", \"artworkUrl100\":\"https://is2-ssl.mzstatic.com/image/thumb/Music6/v4/17/84/3a/17843a6d-1f2b-7e1e-a39f-3ff865110993/source/100x100bb.jpg\", \"collectionPrice\":15.99, \"collectionExplicitness\":\"explicit\", \"contentAdvisoryRating\":\"Explicit\", \"trackCount\":33, \"copyright\":\"℗ 2013 Columbia Records, a Division of Sony Music Entertainment\", \"country\":\"USA\", \"currency\":\"USD\", \"releaseDate\":\"2013-12-13T08:00:00Z\", \"primaryGenreName\":\"Pop\"}]\n" +
                "}\n" +
                "\n" +
                "\n";
    }
    public static String getGoogleBooksResponse() {

        return "{\n" +
                "\"kind\": \"books#volumes\",\n" +
                "\"totalItems\": 796,\n" +
                "\"items\": [\n" +
                "{\n" +
                "\"kind\": \"books#volume\",\n" +
                "\"id\": \"tc53AAAAQBAJ\",\n" +
                "\"etag\": \"PTlalUN/eYw\",\n" +
                "\"selfLink\": \"https://www.googleapis.com/books/v1/volumes/tc53AAAAQBAJ\",\n" +
                "\"volumeInfo\": {\n" +
                "\"title\": \"Animal farm\",\n" +
                "\"authors\": [\n" +
                "\"George Orwell\"\n" +
                "],\n" +
                "\"publisher\": \"Singel Uitgeverijen\",\n" +
                "\"publishedDate\": \"2013-05-16\",\n" +
                "\"description\": \"De dieren op een boerderij komen in opstand tegen hun meester de mens en nemen zelf de macht in handen. De varkens, die lang tevoren in het geheim hebben leren lezen en schrijven, werpen zich op als de natuurlijke leiders van de revolutie. Zij staan immers op een hoger intellectueel peil dan de andere dieren. Ze breiden hun voorrechten steeds verder uit en vormen een nieuwe elite, even oppermachtig als de oude heersers. De catastrofale ineenstorting van deze gemeenschap kan ten slotte niet uitblijven. Animal Farm, geschreven in 1943, is een klassiek geworden satire op een totalitaire staat en samenleving, die vandaag de dag nog niets aan zeggingskracht heeft verloren.\",\n" +
                "\"industryIdentifiers\": [\n" +
                "{\n" +
                "\"type\": \"ISBN_13\",\n" +
                "\"identifier\": \"9789029587747\"\n" +
                "},\n" +
                "{\n" +
                "\"type\": \"ISBN_10\",\n" +
                "\"identifier\": \"9029587741\"\n" +
                "}\n" +
                "],\n" +
                "\"readingModes\": {\n" +
                "\"text\": true,\n" +
                "\"image\": true\n" +
                "},\n" +
                "\"pageCount\": 116,\n" +
                "\"printType\": \"BOOK\",\n" +
                "\"categories\": [\n" +
                "\"Fiction\"\n" +
                "],\n" +
                "\"maturityRating\": \"NOT_MATURE\",\n" +
                "\"allowAnonLogging\": true,\n" +
                "\"contentVersion\": \"1.2.2.0.preview.3\",\n" +
                "\"panelizationSummary\": {\n" +
                "\"containsEpubBubbles\": false,\n" +
                "\"containsImageBubbles\": false\n" +
                "},\n" +
                "\"imageLinks\": {\n" +
                "\"smallThumbnail\": \"http://books.google.com/books/content?id=tc53AAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\n" +
                "\"thumbnail\": \"http://books.google.com/books/content?id=tc53AAAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"\n" +
                "},\n" +
                "\"language\": \"nl\",\n" +
                "\"previewLink\": \"http://books.google.nl/books?id=tc53AAAAQBAJ&printsec=frontcover&dq=george+orwell&hl=&cd=1&source=gbs_api\",\n" +
                "\"infoLink\": \"https://play.google.com/store/books/details?id=tc53AAAAQBAJ&source=gbs_api\",\n" +
                "\"canonicalVolumeLink\": \"https://play.google.com/store/books/details?id=tc53AAAAQBAJ\"\n" +
                "},\n" +
                "\"saleInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"saleability\": \"FOR_SALE\",\n" +
                "\"isEbook\": true,\n" +
                "\"listPrice\": {\n" +
                "\"amount\": 7.99,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amount\": 7.99,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"buyLink\": \"https://play.google.com/store/books/details?id=tc53AAAAQBAJ&rdid=book-tc53AAAAQBAJ&rdot=1&source=gbs_api\",\n" +
                "\"offers\": [\n" +
                "{\n" +
                "\"finskyOfferType\": 1,\n" +
                "\"listPrice\": {\n" +
                "\"amountInMicros\": 7990000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amountInMicros\": 7990000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "}\n" +
                "}\n" +
                "]\n" +
                "},\n" +
                "\"accessInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"viewability\": \"PARTIAL\",\n" +
                "\"embeddable\": true,\n" +
                "\"publicDomain\": false,\n" +
                "\"textToSpeechPermission\": \"ALLOWED\",\n" +
                "\"epub\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/Animal_farm-sample-epub.acsm?id=tc53AAAAQBAJ&format=epub&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"pdf\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/Animal_farm-sample-pdf.acsm?id=tc53AAAAQBAJ&format=pdf&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"webReaderLink\": \"http://play.google.com/books/reader?id=tc53AAAAQBAJ&hl=&printsec=frontcover&source=gbs_api\",\n" +
                "\"accessViewStatus\": \"SAMPLE\",\n" +
                "\"quoteSharingAllowed\": false\n" +
                "},\n" +
                "\"searchInfo\": {\n" +
                "\"textSnippet\": \"Animal Farm, geschreven in 1943, is een klassiek geworden satire op een totalitaire staat en samenleving, die vandaag de dag nog niets aan zeggingskracht heeft verloren.\"\n" +
                "}\n" +
                "},\n" +
                "{\n" +
                "\"kind\": \"books#volume\",\n" +
                "\"id\": \"kotPYEqx7kMC\",\n" +
                "\"etag\": \"tN/aahyXDNc\",\n" +
                "\"selfLink\": \"https://www.googleapis.com/books/v1/volumes/kotPYEqx7kMC\",\n" +
                "\"volumeInfo\": {\n" +
                "\"title\": \"1984\",\n" +
                "\"authors\": [\n" +
                "\"George Orwell\"\n" +
                "],\n" +
                "\"publisher\": \"Houghton Mifflin Harcourt\",\n" +
                "\"publishedDate\": \"1983-10-17\",\n" +
                "\"description\": \"George Orwell’s 1984 takes on new life with extraordinary relevance and renewed popularity. “Orwell saw, to his credit, that the act of falsifying reality is only secondarily a way of changing perceptions. It is, above all, a way of asserting power.”—The New Yorker In 1984, London is a grim city in the totalitarian state of Oceania where Big Brother is always watching you and the Thought Police can practically read your mind. Winston Smith is a man in grave danger for the simple reason that his memory still functions. Drawn into a forbidden love affair, Winston finds the courage to join a secret revolutionary organization called The Brotherhood, dedicated to the destruction of the Party. Together with his beloved Julia, he hazards his life in a deadly match against the powers that be. Lionel Trilling said of Orwell’s masterpiece, “1984 is a profound, terrifying, and wholly fascinating book. It is a fantasy of the political future, and like any such fantasy, serves its author as a magnifying device for an examination of the present.” Though the year 1984 now exists in the past, Orwell’s novel remains an urgent call for the individual willing to speak truth to power.\",\n" +
                "\"industryIdentifiers\": [\n" +
                "{\n" +
                "\"type\": \"ISBN_13\",\n" +
                "\"identifier\": \"9780547249643\"\n" +
                "},\n" +
                "{\n" +
                "\"type\": \"ISBN_10\",\n" +
                "\"identifier\": \"0547249640\"\n" +
                "}\n" +
                "],\n" +
                "\"readingModes\": {\n" +
                "\"text\": true,\n" +
                "\"image\": true\n" +
                "},\n" +
                "\"pageCount\": 648,\n" +
                "\"printType\": \"BOOK\",\n" +
                "\"categories\": [\n" +
                "\"Fiction\"\n" +
                "],\n" +
                "\"averageRating\": 4.5,\n" +
                "\"ratingsCount\": 72,\n" +
                "\"maturityRating\": \"NOT_MATURE\",\n" +
                "\"allowAnonLogging\": true,\n" +
                "\"contentVersion\": \"2.24.21.0.preview.3\",\n" +
                "\"panelizationSummary\": {\n" +
                "\"containsEpubBubbles\": false,\n" +
                "\"containsImageBubbles\": false\n" +
                "},\n" +
                "\"imageLinks\": {\n" +
                "\"smallThumbnail\": \"http://books.google.com/books/content?id=kotPYEqx7kMC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\n" +
                "\"thumbnail\": \"http://books.google.com/books/content?id=kotPYEqx7kMC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"\n" +
                "},\n" +
                "\"language\": \"en\",\n" +
                "\"previewLink\": \"http://books.google.nl/books?id=kotPYEqx7kMC&printsec=frontcover&dq=george+orwell&hl=&cd=2&source=gbs_api\",\n" +
                "\"infoLink\": \"https://play.google.com/store/books/details?id=kotPYEqx7kMC&source=gbs_api\",\n" +
                "\"canonicalVolumeLink\": \"https://play.google.com/store/books/details?id=kotPYEqx7kMC\"\n" +
                "},\n" +
                "\"saleInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"saleability\": \"FOR_SALE\",\n" +
                "\"isEbook\": true,\n" +
                "\"listPrice\": {\n" +
                "\"amount\": 12.69,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amount\": 8.63,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"buyLink\": \"https://play.google.com/store/books/details?id=kotPYEqx7kMC&rdid=book-kotPYEqx7kMC&rdot=1&source=gbs_api\",\n" +
                "\"offers\": [\n" +
                "{\n" +
                "\"finskyOfferType\": 1,\n" +
                "\"listPrice\": {\n" +
                "\"amountInMicros\": 12690000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amountInMicros\": 8630000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "}\n" +
                "}\n" +
                "]\n" +
                "},\n" +
                "\"accessInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"viewability\": \"PARTIAL\",\n" +
                "\"embeddable\": true,\n" +
                "\"publicDomain\": false,\n" +
                "\"textToSpeechPermission\": \"ALLOWED\",\n" +
                "\"epub\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/1984-sample-epub.acsm?id=kotPYEqx7kMC&format=epub&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"pdf\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/1984-sample-pdf.acsm?id=kotPYEqx7kMC&format=pdf&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"webReaderLink\": \"http://play.google.com/books/reader?id=kotPYEqx7kMC&hl=&printsec=frontcover&source=gbs_api\",\n" +
                "\"accessViewStatus\": \"SAMPLE\",\n" +
                "\"quoteSharingAllowed\": false\n" +
                "},\n" +
                "\"searchInfo\": {\n" +
                "\"textSnippet\": \"A PBS Great American Read Top 100 Pick With extraordinary relevance and renewed popularity, George Orwell’s 1984 takes on new life in this edition. “Orwell saw, to his credit, that the act of falsifying reality is only secondarily a way ...\"\n" +
                "}\n" +
                "},\n" +
                "{\n" +
                "\"kind\": \"books#volume\",\n" +
                "\"id\": \"zaxG_3ivhVAC\",\n" +
                "\"etag\": \"rKj8XFfxZKU\",\n" +
                "\"selfLink\": \"https://www.googleapis.com/books/v1/volumes/zaxG_3ivhVAC\",\n" +
                "\"volumeInfo\": {\n" +
                "\"title\": \"George Orwell: In front of your nose, 1946-1950\",\n" +
                "\"authors\": [\n" +
                "\"George Orwell\",\n" +
                "\"Sonia Orwell\",\n" +
                "\"Ian Angus\"\n" +
                "],\n" +
                "\"publisher\": \"David R. Godine Publisher\",\n" +
                "\"publishedDate\": \"2000-01-01\",\n" +
                "\"industryIdentifiers\": [\n" +
                "{\n" +
                "\"type\": \"ISBN_10\",\n" +
                "\"identifier\": \"1567921361\"\n" +
                "},\n" +
                "{\n" +
                "\"type\": \"ISBN_13\",\n" +
                "\"identifier\": \"9781567921366\"\n" +
                "}\n" +
                "],\n" +
                "\"readingModes\": {\n" +
                "\"text\": false,\n" +
                "\"image\": false\n" +
                "},\n" +
                "\"pageCount\": 555,\n" +
                "\"printType\": \"BOOK\",\n" +
                "\"categories\": [\n" +
                "\"Literary Collections\"\n" +
                "],\n" +
                "\"averageRating\": 4.5,\n" +
                "\"ratingsCount\": 3,\n" +
                "\"maturityRating\": \"NOT_MATURE\",\n" +
                "\"allowAnonLogging\": false,\n" +
                "\"contentVersion\": \"1.0.0.0.preview.0\",\n" +
                "\"imageLinks\": {\n" +
                "\"smallThumbnail\": \"http://books.google.com/books/content?id=zaxG_3ivhVAC&printsec=frontcover&img=1&zoom=5&source=gbs_api\",\n" +
                "\"thumbnail\": \"http://books.google.com/books/content?id=zaxG_3ivhVAC&printsec=frontcover&img=1&zoom=1&source=gbs_api\"\n" +
                "},\n" +
                "\"language\": \"en\",\n" +
                "\"previewLink\": \"http://books.google.nl/books?id=zaxG_3ivhVAC&dq=george+orwell&hl=&cd=3&source=gbs_api\",\n" +
                "\"infoLink\": \"http://books.google.nl/books?id=zaxG_3ivhVAC&dq=george+orwell&hl=&source=gbs_api\",\n" +
                "\"canonicalVolumeLink\": \"https://books.google.com/books/about/George_Orwell_In_front_of_your_nose_1946.html?hl=&id=zaxG_3ivhVAC\"\n" +
                "},\n" +
                "\"saleInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"saleability\": \"NOT_FOR_SALE\",\n" +
                "\"isEbook\": false\n" +
                "},\n" +
                "\"accessInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"viewability\": \"NO_PAGES\",\n" +
                "\"embeddable\": false,\n" +
                "\"publicDomain\": false,\n" +
                "\"textToSpeechPermission\": \"ALLOWED\",\n" +
                "\"epub\": {\n" +
                "\"isAvailable\": false\n" +
                "},\n" +
                "\"pdf\": {\n" +
                "\"isAvailable\": false\n" +
                "},\n" +
                "\"webReaderLink\": \"http://play.google.com/books/reader?id=zaxG_3ivhVAC&hl=&printsec=frontcover&source=gbs_api\",\n" +
                "\"accessViewStatus\": \"NONE\",\n" +
                "\"quoteSharingAllowed\": false\n" +
                "},\n" +
                "\"searchInfo\": {\n" +
                "\"textSnippet\": \"Considering that much of his life was spent in poverty and ill health, it is something of a miracle that in only forty-six years George Orwell managed to publish ten books and two collections of essays.\"\n" +
                "}\n" +
                "},\n" +
                "{\n" +
                "\"kind\": \"books#volume\",\n" +
                "\"id\": \"VO8nDwAAQBAJ\",\n" +
                "\"etag\": \"ka3pn/ZvfUA\",\n" +
                "\"selfLink\": \"https://www.googleapis.com/books/v1/volumes/VO8nDwAAQBAJ\",\n" +
                "\"volumeInfo\": {\n" +
                "\"title\": \"1984\",\n" +
                "\"authors\": [\n" +
                "\"George Orwell\"\n" +
                "],\n" +
                "\"publisher\": \"Enrich Spot Limited\",\n" +
                "\"publishedDate\": \"2016-07-29\",\n" +
                "\"description\": \"1984 is a dystopian novel by English author George Orwell published in 1949. The novel is set in Airstrip One, a world of perpetual war, omnipresent government surveillance, and public manipulation. It is dictated by a political system named English Socialism under the control of the Inner Party, that persecutes individualism and independent thinking. Many of its terms and concepts, such as Big Brother, doublethink, thoughtcrime, Newspeak, Room 101, telescreen, 2 + 2 = 5, and memory holes, have entered into common use since its publication. In 2005, the novel was chosen by TIME magazine as one of the 100 best English-language novels.\",\n" +
                "\"industryIdentifiers\": [\n" +
                "{\n" +
                "\"type\": \"ISBN_13\",\n" +
                "\"identifier\": \"9789881235602\"\n" +
                "},\n" +
                "{\n" +
                "\"type\": \"ISBN_10\",\n" +
                "\"identifier\": \"988123560X\"\n" +
                "}\n" +
                "],\n" +
                "\"readingModes\": {\n" +
                "\"text\": false,\n" +
                "\"image\": true\n" +
                "},\n" +
                "\"printType\": \"BOOK\",\n" +
                "\"categories\": [\n" +
                "\"Juvenile Fiction\"\n" +
                "],\n" +
                "\"averageRating\": 4,\n" +
                "\"ratingsCount\": 1786,\n" +
                "\"maturityRating\": \"NOT_MATURE\",\n" +
                "\"allowAnonLogging\": true,\n" +
                "\"contentVersion\": \"preview-1.0.0\",\n" +
                "\"panelizationSummary\": {\n" +
                "\"containsEpubBubbles\": false,\n" +
                "\"containsImageBubbles\": false\n" +
                "},\n" +
                "\"imageLinks\": {\n" +
                "\"smallThumbnail\": \"http://books.google.com/books/content?id=VO8nDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\n" +
                "\"thumbnail\": \"http://books.google.com/books/content?id=VO8nDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"\n" +
                "},\n" +
                "\"language\": \"en\",\n" +
                "\"previewLink\": \"http://books.google.nl/books?id=VO8nDwAAQBAJ&printsec=frontcover&dq=george+orwell&hl=&cd=4&source=gbs_api\",\n" +
                "\"infoLink\": \"https://play.google.com/store/books/details?id=VO8nDwAAQBAJ&source=gbs_api\",\n" +
                "\"canonicalVolumeLink\": \"https://play.google.com/store/books/details?id=VO8nDwAAQBAJ\"\n" +
                "},\n" +
                "\"saleInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"saleability\": \"FOR_SALE\",\n" +
                "\"isEbook\": true,\n" +
                "\"listPrice\": {\n" +
                "\"amount\": 19.06,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amount\": 13.34,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"buyLink\": \"https://play.google.com/store/books/details?id=VO8nDwAAQBAJ&rdid=book-VO8nDwAAQBAJ&rdot=1&source=gbs_api\",\n" +
                "\"offers\": [\n" +
                "{\n" +
                "\"finskyOfferType\": 1,\n" +
                "\"listPrice\": {\n" +
                "\"amountInMicros\": 19060000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "},\n" +
                "\"retailPrice\": {\n" +
                "\"amountInMicros\": 13340000,\n" +
                "\"currencyCode\": \"EUR\"\n" +
                "}\n" +
                "}\n" +
                "]\n" +
                "},\n" +
                "\"accessInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"viewability\": \"PARTIAL\",\n" +
                "\"embeddable\": true,\n" +
                "\"publicDomain\": false,\n" +
                "\"textToSpeechPermission\": \"ALLOWED\",\n" +
                "\"epub\": {\n" +
                "\"isAvailable\": false\n" +
                "},\n" +
                "\"pdf\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/1984-sample-pdf.acsm?id=VO8nDwAAQBAJ&format=pdf&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"webReaderLink\": \"http://play.google.com/books/reader?id=VO8nDwAAQBAJ&hl=&printsec=frontcover&source=gbs_api\",\n" +
                "\"accessViewStatus\": \"SAMPLE\",\n" +
                "\"quoteSharingAllowed\": false\n" +
                "},\n" +
                "\"searchInfo\": {\n" +
                "\"textSnippet\": \"1984 is a dystopian novel by English author George Orwell published in 1949. ?The novel is set in Airstrip One, a world of perpetual war, omnipresent government surveillance, and public manipulation.\"\n" +
                "}\n" +
                "},\n" +
                "{\n" +
                "\"kind\": \"books#volume\",\n" +
                "\"id\": \"WQwlYM9GcXAC\",\n" +
                "\"etag\": \"ftJitC4/SIA\",\n" +
                "\"selfLink\": \"https://www.googleapis.com/books/v1/volumes/WQwlYM9GcXAC\",\n" +
                "\"volumeInfo\": {\n" +
                "\"title\": \"George Orwell, Updated Edition\",\n" +
                "\"authors\": [\n" +
                "\"Harold Bloom\"\n" +
                "],\n" +
                "\"publisher\": \"Infobase Publishing\",\n" +
                "\"publishedDate\": \"2009-01-01\",\n" +
                "\"description\": \"George Orwell wrote many essays and political pamphlets, yet most know him for his fable Animal Farm and his dystopian novel 1984. The essays in this enhanced Bloom's Modern Critical Views volume offer precise commentary on Orwell's p\",\n" +
                "\"industryIdentifiers\": [\n" +
                "{\n" +
                "\"type\": \"ISBN_13\",\n" +
                "\"identifier\": \"9781438113005\"\n" +
                "},\n" +
                "{\n" +
                "\"type\": \"ISBN_10\",\n" +
                "\"identifier\": \"1438113005\"\n" +
                "}\n" +
                "],\n" +
                "\"readingModes\": {\n" +
                "\"text\": true,\n" +
                "\"image\": true\n" +
                "},\n" +
                "\"printType\": \"BOOK\",\n" +
                "\"maturityRating\": \"NOT_MATURE\",\n" +
                "\"allowAnonLogging\": false,\n" +
                "\"contentVersion\": \"0.0.1.0.preview.3\",\n" +
                "\"imageLinks\": {\n" +
                "\"smallThumbnail\": \"http://books.google.com/books/content?id=WQwlYM9GcXAC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\n" +
                "\"thumbnail\": \"http://books.google.com/books/content?id=WQwlYM9GcXAC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"\n" +
                "},\n" +
                "\"language\": \"en\",\n" +
                "\"previewLink\": \"http://books.google.nl/books?id=WQwlYM9GcXAC&printsec=frontcover&dq=george+orwell&hl=&cd=5&source=gbs_api\",\n" +
                "\"infoLink\": \"http://books.google.nl/books?id=WQwlYM9GcXAC&dq=george+orwell&hl=&source=gbs_api\",\n" +
                "\"canonicalVolumeLink\": \"https://books.google.com/books/about/George_Orwell_Updated_Edition.html?hl=&id=WQwlYM9GcXAC\"\n" +
                "},\n" +
                "\"saleInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"saleability\": \"NOT_FOR_SALE\",\n" +
                "\"isEbook\": false\n" +
                "},\n" +
                "\"accessInfo\": {\n" +
                "\"country\": \"NL\",\n" +
                "\"viewability\": \"PARTIAL\",\n" +
                "\"embeddable\": true,\n" +
                "\"publicDomain\": false,\n" +
                "\"textToSpeechPermission\": \"ALLOWED\",\n" +
                "\"epub\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/George_Orwell_Updated_Edition-sample-epub.acsm?id=WQwlYM9GcXAC&format=epub&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"pdf\": {\n" +
                "\"isAvailable\": true,\n" +
                "\"acsTokenLink\": \"http://books.google.nl/books/download/George_Orwell_Updated_Edition-sample-pdf.acsm?id=WQwlYM9GcXAC&format=pdf&output=acs4_fulfillment_token&dl_type=sample&source=gbs_api\"\n" +
                "},\n" +
                "\"webReaderLink\": \"http://play.google.com/books/reader?id=WQwlYM9GcXAC&hl=&printsec=frontcover&source=gbs_api\",\n" +
                "\"accessViewStatus\": \"SAMPLE\",\n" +
                "\"quoteSharingAllowed\": false\n" +
                "},\n" +
                "\"searchInfo\": {\n" +
                "\"textSnippet\": \"Presents a collection of critical essays on the works of George Orwell.\"\n" +
                "}\n" +
                "}\n" +
                "]\n" +
                "}" ;
    }

    public static String getEmptyITunesResponse() {
        return "{\n" +
                " \"resultCount\":0,\n" +
                " \"results\": []\n" +
                "}" ;
    }
    public static String getEmptyGoogleResponse() {
        return "{\n" +
                "\"kind\": \"books#volumes\",\n" +
                "\"totalItems\": 0\n" +
                "}" ;
    }
}
