package com.idexx.bookmusicassignment.resource;

import com.idexx.bookmusicassignment.domain.Book;
import com.idexx.bookmusicassignment.domain.MusicAlbum;
import com.idexx.bookmusicassignment.service.BookService;
import com.idexx.bookmusicassignment.service.MusicAlbumService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StoreResourceTest {

    @Mock
    private BookService bookService;
    @Mock
    private MusicAlbumService musicAlbumService;

    @InjectMocks
    private StoreResource storeResource;

    @Test
    public void getResult() throws IOException {
        String input = "George Orwell";

        List<Book> books = Arrays.asList(Book.builder().title("George Orwell biography").build());
        when(bookService.getBooks(input)).thenReturn(books);
        List<MusicAlbum> albums = Arrays.asList(MusicAlbum.builder().title("George Orwell world tour").build());
        when(musicAlbumService.getMusicAlbums(input)).thenReturn(albums);
        ResultDto result = storeResource.getStoreResults("George Orwell");

        assertEquals(1, result.getAlbums().size());
        assertEquals(1, result.getBooks().size());
    }
}
